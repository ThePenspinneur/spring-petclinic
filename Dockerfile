FROM gcr.io/distroless/java:11
WORKDIR /app
COPY target/spring-petclinic-*-SNAPSHOT.jar main.jar
CMD ["main.jar"]
EXPOSE 8080
